<?php
namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "product";
	protected $guarded = array('id');
	public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
