<?php
namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
	protected $guarded = array('id');

	public function products()
    {
        return $this->hasMany('Product');
    }
}
