<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('welcome');
    return 'Hello user....';
});
Route::group(array('prefix' => 'api/v1'), function()
{
	Route::resource('product', 'productController');
	Route::get('search', 'productController@search');
	Route::resource('category', 'categoryController');
	Route::resource('registration', 'registrationController');
});
Route::get('csrf', function() {
    return Session::token();
});