<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Registration;
use App\Category;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		$this->call('ProductTableSeeder');		
		$this->call('CategoryTableSeeder');	
		$this->call('RegistrationTableSeeder');			
	}
}

class ProductTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('product')->delete();
		Product::create( 
				array(
					'category_id' => '1' ,  
					'product_name' => 'Shirt' , 
					'product_desc' => 'This is the shirt.' ,
					'product_modal' => 'SH1',
					'qty' => '10',
					'price' => '250'
					) );

		Product::create( 
				array( 
					'category_id' => '2' ,  
					'product_name' => 'T-shirt' , 
					'product_desc' => 'This is the T-shirt.' ,
					'product_modal' => 'TS1',
					'qty' => '10',
					'price' => '200'
					) );

		Product::create( 
				array( 
					'category_id' => '1' ,  
					'product_name' => 'Jeans' , 
					'product_desc' => 'This is the Jeans.' ,
					'product_modal' => 'J1',
					'qty' => '10',
					'price' => '550'
					) );

		Product::create( 
				array( 
					'category_id' => '2' ,  
					'product_name' => 'Trouser' , 
					'product_desc' => 'This is the trouser.' ,
					'product_modal' => 'TS1',
					'qty' => '10',
					'price' => '450'
					) );
	}
}

class CategoryTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('category')->delete();
		Category::create( 
				array( 
					'category_name' => 'CAT1' , 
					'category_desc' => 'This is the category 1.' ,
					) );

		Category::create( 
				array( 
					'category_name' => 'CAT2' , 
					'category_desc' => 'This is the category 2.' ,
					) );
	}
}

class RegistrationTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('registration')->delete();
		Registration::create( 
				array( 
					'first_name' => 'Jigar' , 
					'last_name' => 'Patel' ,
					'email' => 'jigar.patel8086@gmail.com',
					'address_line1' => 'Jawahar chowk',
					'address_line2' => 'Maninagar',
					'city' => 'Ahmedabad',
					'state' => 'Gujarat',
					'country' => 'India',
					'pincode' => '380008',
					) );

		Registration::create( 
				array( 
					'first_name' => 'Parth' , 
					'last_name' => 'Bhagat' ,
					'email' => 'parthbhagat@gmail.com',
					'address_line1' => 'kankariya',
					'address_line2' => 'Kankariya',
					'city' => 'Ahmedabad',
					'state' => 'Gujarat',
					'country' => 'India',
					'pincode' => '380008',
					) );
	}
}


